import java.util.Scanner;

class MyCalculator 
{
	long power(int a, int b) throws Exception
	{
		long r=0;
		if(a<0 || b<0)
		{
			throw new Exception("a or b cannt be negative");
			
		}
		if(a==0 && b==0)
		{
			throw new Exception("a and b cannt be zero");
			
		}			
		r=(long) Math.pow(a,b);
		return r;
	}
}
public class Prog3
{

	public static void main(String[] args)
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Enter two numbers");
		int a=s.nextInt();
		int b=s.nextInt();
		MyCalculator m=new MyCalculator();
		try
		{		
		    System.out.println(m.power(a,b));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}

}
