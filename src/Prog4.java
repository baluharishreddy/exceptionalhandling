import java.util.Scanner;
class CustomException extends Exception
{
	CustomException(String message)
	{
		super(message);
	}
}

class Age 
{
	int AgeMethod(int a) throws CustomException
	{
	   if(a<19)
		  throw new CustomException("Invalid Age");
	   return a;
	}
}
public class Prog4 
{

	public static void main(String[] args) 
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Player name");
		String name=s.nextLine();
		System.out.println("Enter age");		
		int a=s.nextInt();
		Age age=new Age();
		try
		{
			age.AgeMethod(a);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		if(a>=19)
		{
		    System.out.println(name);
		    System.out.println(a);
		} 
	}

}
